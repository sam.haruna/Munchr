package ca.maavis.munchr;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import helpers.DisplayAlertDialog;

import library.DatabaseHandler;
import library.UserFunctions;
import helpers.ConnectionDetector;
import helpers.GPSChecker;

public class LoginActivity extends Activity {
	Button btnLogin;
    Button btnLinkToRegister;
    EditText inputEmail;
    EditText inputPassword;
    TextView loginErrorMsg;
    DisplayAlertDialog alertDialog = new DisplayAlertDialog();
    ProgressDialog pDialog;
    
    private static final String KEY_CATEGORY = "category";
    private static final String KEY_ITEM = "item";
    private static final String KEY_PRICE = "price";
    
    //flag for internet connection
    private Boolean isInternetPresent = false;
    private Boolean gotLocation = false;
    
    // Connection detector class
    private  ConnectionDetector cd;
    //GPS checker
    private GPSChecker GPS;

    private void checkConnectionAndLocation(ConnectionDetector cd, GPSChecker gps){
    	isInternetPresent = cd.isConnectedToInternet();
    	gotLocation = gps.isLocationDetected();
    } 
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
         }
		
		// Importing all assets like buttons, text fields
        inputEmail = (EditText) findViewById(R.id.loginEmail);
        inputPassword = (EditText) findViewById(R.id.loginPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLinkToRegister = (Button) findViewById(R.id.btnLinkToRegisterScreen);
        loginErrorMsg = (TextView) findViewById(R.id.login_error);
        
        //Check if user has an Internet connection and determine user location
        cd = new ConnectionDetector(getApplicationContext());
        GPS = new GPSChecker(this);
        
        // TODO
        /*
         * Place these conditions in a loop to poll for internet connection and GPS coordinates.
         * Wait for the user to turn on GPS.
         * Place loop in a time interval to ensure application does not drain battery 
         */
        checkConnectionAndLocation(cd, GPS);
        
        if (!isInternetPresent){
        	alertDialog.showAlertDialog(LoginActivity.this, "There is no Internet connection",null, false);
        }
        else if(!gotLocation){
        	alertDialog.showAlertDialog(LoginActivity.this, "Could not determine your location",null, false);
        }
        else if (!isInternetPresent && !gotLocation){
        	alertDialog.showAlertDialog(LoginActivity.this, "There is No Internet Connection & could not determine your location",null, false);
        }
        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener() {
 
            public void onClick(View view) {
                /*String email = inputEmail.getText().toString();
                String password = inputPassword.getText().toString();*/
                
                UserFunctions userFunction = new UserFunctions();
                DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                HashMap<String, String> user = db.getUserDetails();
                //JSONObject json_user = json.getJSONObject("user");
                //JSONObject json_menu = json.getJSONObject("menu");
                if(user!= null){
                	/*
                	String name = user.get("name");
                    String email_db = user.get("email");
                    String uid = user.get("uid");
                    String created = user.get("created_at");
                    String passwd = "";*/
                    
                    // Clear all previous data in database
                    userFunction.logoutUser(getApplicationContext());
                    //db.addUser(name, email_db, password, uid, created);

                    JSONObject json = userFunction.getMenu();
                    try {
						JSONObject json_menu = json.getJSONObject("menu");
						
						int i = 0;
	                    String item = "item 0";
	                    while(i < json_menu.length()){
	                    	JSONObject json_menu_item = json_menu.getJSONObject(item);
	                    	db.addMenu(json_menu_item.getString(KEY_CATEGORY), json_menu_item.getString(KEY_ITEM), json_menu_item.getDouble(KEY_PRICE));
	                    	item = "item " + Integer.toString(i);
	                    	i++;
	                    }
	                    
	                    // Launch Dash board Screen
	                    Intent dashboard = new Intent(getApplicationContext(), DashboardActivity.class);
	                    // Close all views before launching Dash board
	                    dashboard.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	                    startActivity(dashboard);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                }
                else{
                	// Launch Dash board Screen
                    Intent dashboard = new Intent(getApplicationContext(), RegisterActivity.class);
                    // Close all views before launching Dash board
                    dashboard.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(dashboard);
                }
             
            }
        });
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_login, menu);
		return true;
	}

}
