package ca.maavis.munchr;


import java.util.ArrayList;

import library.DatabaseHandler;
import library.UserFunctions;

import helpers.ConnectionDetector;
import helpers.DisplayAlertDialog;
import helpers.PlaceInfo;
import helpers.PlacesFound;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
 

public class SinglePlaceActivity extends Activity {
	
	ConnectionDetector cDetector; 	
	
	DisplayAlertDialog alertDialog = new DisplayAlertDialog();	
	
	Boolean internetAvailable= false;
	
	ProgressDialog progressDialog;

	PlacesFound googlePlaces;
	
	PlaceInfo placeDetails;
	
	public static String CATEGORY="category"; //place id
	
	public static String ITEM= "item";// place name 
	
	public static String PRICE="price";//place area
	
	ArrayList<String> foodItem = new ArrayList<String>();
	
	public static String REFERENCE = "reference"; //place id
	
	UserFunctions userFunctions;
    DatabaseHandler db;
	   @Override
	 protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_singleplace);
	   		userFunctions = new UserFunctions();
       		db = new DatabaseHandler(getApplicationContext());
       		Intent intent = getIntent();
	   		
	   		final Button delivery_button = (Button) findViewById(R.id.delivery_button);
	         delivery_button.setOnClickListener(new View.OnClickListener() {
	             public void onClick(View v) {
	            	// Perform action on click
	            	 Bundle bundle = new Bundle();
	            	 String b = "1";
	            	 bundle.putString("ONE", b);
	            	 //String reference = ((TextView) v.findViewById(R.id.reference)).getText().toString();
	            	 Intent intent = new Intent(getApplicationContext(), MenuActivity.class);	
	 				 //intent.putExtra(REFERENCE, reference);
	 				 startActivity(intent);
	             }
	         });
	         final Button pickup_button = (Button) findViewById(R.id.pickup_button);
	         pickup_button.setOnClickListener(new View.OnClickListener() {
	             public void onClick(View v) {
	                 // Perform action on click
	            	 Intent intent = new Intent(getApplicationContext(), MenuActivity.class);	
	 				 //intent.putExtra(REFERENCE, reference);
	 				 startActivity(intent);
	             }
	         });
	         final Button reserve_button = (Button) findViewById(R.id.reserve_button);
	         reserve_button.setOnClickListener(new View.OnClickListener() {
	             public void onClick(View v) {
	                 // Perform action on click
	             }
	         });
	   		String reference = intent.getStringExtra(REFERENCE);// Place  id
	        // Calling a Async Background thread
	        new LoadPlaceInfo().execute(reference);
	       
	    }
	   class LoadPlaceInfo extends AsyncTask<String, String, String> {
		   @Override
			protected void onPreExecute() {
				super.onPreExecute();
				progressDialog = new ProgressDialog(SinglePlaceActivity.this);
				progressDialog.setMessage("Loading restaurant details ...");
				progressDialog.setIndeterminate(false);
				progressDialog.setCancelable(false);
				progressDialog.show();
			}
		   protected String doInBackground(String... args) {
			String reference = args[0];
			
			googlePlaces= new PlacesFound();
			
			try{
				placeDetails = googlePlaces.getPlaceDetails(reference);
			}	catch (Exception e) {
				e.printStackTrace();
			}
			
			return null;
		   }

		   protected void onPostExecute(String file_url) {
			   progressDialog.dismiss();
			   runOnUiThread(new Runnable() 
			   {
				   public void run(){
					   
					   if(placeDetails!=null){
						   
						   if(placeDetails.status.equals("OK")) {
							   	if(placeDetails.result!=null) {
								   String name = placeDetails.result.name;
								   String address= placeDetails.result.formatted_address;
								   String phone=placeDetails.result.formatted_phone_number;
								   String rating = String.valueOf(placeDetails.result.rating);
								   
	                                TextView PlaceName = (TextView) findViewById(R.id.name);
	                                TextView PlaceAddress = (TextView) findViewById(R.id.address);
	                                TextView PlacePhone = (TextView) findViewById(R.id.phone);
	                                TextView PlaceRating = (TextView) findViewById(R.id.rating);
	                                PlaceName.setText(name);
	                                PlaceAddress.setText(address);
	                                PlacePhone.setText(Html.fromHtml("<b>Phone Number:</b> " + phone));
	                                PlaceRating.setText(rating);
								
							   	  }
							}
							else if(placeDetails.status.equals("ZERO_RESULTS")){
								alertDialog.showAlertDialog(SinglePlaceActivity.this, "Nearby restaurents",
										"no Restaurents found!.",
										false);
							}
							else if(placeDetails.status.equals("UNKNOWN_ERROR"))
							{
								alertDialog.showAlertDialog(SinglePlaceActivity.this, " Error",
										"unknown error occured.",
										false);
							}
							else if(placeDetails.status.equals("OVER_QUERY_LIMIT"))
							{
								alertDialog.showAlertDialog(SinglePlaceActivity.this, " Error",
										" Over query limit",
										false);
							}
							else if(placeDetails.status.equals("REQUEST_DENIED"))
							{
								alertDialog.showAlertDialog(SinglePlaceActivity.this, "Error",
										" Request denied",
										false);
							}
							else if(placeDetails.status.equals("INVALID_REQUEST"))
							{
								alertDialog.showAlertDialog(SinglePlaceActivity.this, " Error",
										"Invalid Request",
										false);
							}
							else
							{
								alertDialog.showAlertDialog(SinglePlaceActivity.this, "Error",
										" error occured.",
										false);
							}

						}
						
						
					}
				});

			}

		}

	}

